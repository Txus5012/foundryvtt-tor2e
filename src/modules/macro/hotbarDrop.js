export default function () {
    /**
     * Create a macro when dropping an entity on the hotbar
     * Item      - open roll dialog for item
     * Actor     - open actor sheet
     * Journal   - open journal sheet
     */
    Hooks.on("hotbarDrop", async (bar, data, slot) => {
        // Create item macro if rollable item - weapon, spell, prayer, trait, or skill
        if (data.type === "Item") {
            const item = game.items.get(data.id);
            let itemType = item?.data?.type ?? data?.data?.type;
            let itemName = item?.name ?? data?.data?.name;
            let itemImg = item?.img ?? data?.data?.img;
            if (itemType !== "weapon" && itemType !== "trait" && itemType !== "skill")
                return;
            if (itemType === "weapon") {
                let command = `game.tor2e.macro.utility.rollItemMacro("${itemName}", "${itemType}");`;
                const entities = Array.from(game.macros);
                let macro = entities.find(m => (m.name === itemName) && (m.data.command === command));
                if (!macro) {
                    macro = await Macro.create({
                        name: itemName,
                        type: "script",
                        img: itemImg,
                        command: command
                    }, {displaySheet: false})
                }
                game.user.assignHotbarMacro(macro, slot);
                return;
            }
        }
        // Create a macro to open the actor sheet of the actor dropped on the hotbar
        else if (data.type === "Actor") {
            const actor = game.actors.get(data.id);
            let command = `game.actors.get("${data.id}").sheet.render(true)`
            const entities = Array.from(game.macros);
            let macro = entities.find(m => (m.name === actor.name) && (m.data.command === command));
            if (!macro) {
                macro = await Macro.create({
                    name: actor.data.name,
                    type: "script",
                    img: actor.data.img,
                    command: command
                }, {displaySheet: false})
            }
            game.user.assignHotbarMacro(macro, slot);
        }
        // Create a macro to open the journal sheet of the journal dropped on the hotbar
        else if (data.type === "JournalEntry") {
            let journal = game.journal.get(data.id);
            let command = `game.journal.get("${data.id}").sheet.render(true)`
            const entities = Array.from(game.macros);
            let macro = entities.find(m => (m.name === journal.name) && (m.data.command === command));
            if (!macro) {
                macro = await Macro.create({
                    name: journal.data.name,
                    type: "script",
                    img: "systems/tor2e/assets/images/icons/skill.png",
                    command: command
                }, {displaySheet: false})
            }
            game.user.assignHotbarMacro(macro, slot);
        }
        return false;
    });
}
