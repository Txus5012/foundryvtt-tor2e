import {tor2eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor2eAdversarySheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "actor"],
            width: 650,
            height: 775,
            template: `${CONFIG.tor2e.properties.rootpath}/templates/sheets/actors/adversary-sheet.hbs`
        });
    }

    getData() {
        const baseData = super.getData();
        let constants = CONFIG.tor2e.constants;

        let items = baseData.items.map(i => this.actor.items.get(i._id));

        let [leftFellAbilities, rightFellAbilities] =
            tor2eUtilities.filtering.getAndSplitItemsBy(items, constants.fellAbility);
        let [leftDistinctiveFeatures, rightDistinctiveFeatures] = tor2eUtilities.filtering.getAndSplitItemsBy(items, constants.trait, constants.distinctiveFeature);
        let shield = tor2eUtilities.filtering.getItemBy(items, constants.armour, constants.shield);
        let headgear = tor2eUtilities.filtering.getItemBy(items, constants.armour, constants.headgear);
        let armour = tor2eUtilities.filtering.getItemIn(items, constants.armour, [constants.mailArmour, constants.leatherArmour]);

        let headGearValue = headgear ? headgear.data.data.protection.value : 0;
        let armourValue = armour ? armour.data.data.protection.value : 0;
        return {
            useSkillBlock:  game.settings.get("tor2e", "useAdversarySkills"),
            owner: this.actor.isOwner,
            data: baseData.actor.data.data,
            actor: baseData.actor,
            config: CONFIG.tor2e,
            backgroundImages: CONFIG.tor2e.backgroundImages["adversary"],
            leftFellAbilities: leftFellAbilities,
            rightFellAbilities: rightFellAbilities,
            leftDistinctiveFeatures: leftDistinctiveFeatures,
            rightDistinctiveFeatures: rightDistinctiveFeatures,
            weapons: tor2eUtilities.filtering.getItemsBy(items, constants.weapon),
            shieldDTO: {
                id: shield ? shield.id : 0,
                name: shield ? shield.name : game.i18n.localize("tor2e.actors.stats.noShield"),
                value: shield ? shield.data.data.protection.value : 0,
            },
            headgearDTO: {
                id: headgear ? headgear.id : 0,
                css: "dice",
                name: headgear ? headgear.name : game.i18n.localize("tor2e.actors.stats.noHeadgear"),
                value: headGearValue,
            },
            armourDTO: {
                id: armour ? armour.id : 0,
                css: "dice",
                name: armour ? armour.name : game.i18n.localize("tor2e.actors.stats.noArmour"),
                value: armourValue,
                protectionTotalValue: armourValue + headGearValue,
                favoured: {
                    state: this.actor.data.data.armour.favoured.value,
                    name: "data.armour.favoured.value",
                    value: 0
                },
                roll: {
                    label: "tor2e.rolls.protection",
                    associatedAttribute: "strength",
                    bonus: this?.actor?.data?.extendedData?.getProtectionRollModifier() ?? 0
                }
            },
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
            }
        };
    }

    /**
     * Sets up the data transfer within a drag and drop event. This function is triggered
     * when the user starts dragging an inventory item, and dataTransfer is set to the
     * relevant data needed by the _onDrop function. See that for how drop events
     * are handled.
     *
     * @private
     *
     * @param {Object} event    event triggered by item dragging
     */
    _onDragCombatSkillStart(event) {
        let itemId = event.currentTarget.getAttribute("data-item-id");
        const item = duplicate(this.actor.items.get(itemId))
        event.dataTransfer.setData("text/plain", JSON.stringify({
            type: "Item",
            actorId: this.actor.id,
            data: item,
        }));
    }

    activateListeners(html) {
        super.activateListeners(html);

        // Combat Dragging
        let combatSkillHandler = ev => this._onDragCombatSkillStart(ev);
        html.find('.item-draggable').each((i, li) => {
            li.setAttribute("draggable", true);
            li.addEventListener("dragstart", combatSkillHandler, false);
        });

        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor2eEffect").click(tor2eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".item-delete").click(tor2eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor2eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor2eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor2eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor2eUtilities.eventsProcessing.onEditorToggle.bind(this));


        // Owner-only listeners
        if (this.actor.isOwner) {
            let extra = {
                "shadowServant": true,
            }
            html.find(".inline-skill-modify").click(tor2eUtilities.eventsProcessing.onSkillModify.bind(this, {}));
            html.find(".inline-item-skill-modify").click(tor2eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));
            html.find(".skill-name").click(tor2eUtilities.eventsProcessing.onSkillName.bind(this, extra));
            html.find(".weapon-name").click(tor2eUtilities.eventsProcessing.onItemName.bind(this, extra));
        }

    }

}