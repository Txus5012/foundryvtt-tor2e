import {tor2eUtilities} from "./utilities.js";
import * as Dice from "./sheets/dice.js";
import {StatusEffects} from "./effects/status-effects.js";
import Tor2eChatMessage from "./chat/Tor2eChatMessage.js";
import Tor2eCharacterSpecialSuccess from "./roll/Tor2eCharacterSpecialSuccess.js";
import Tor2eAdversarySpecialSuccess from "./roll/Tor2eAdversarySpecialSuccess.js";
import {Tor2eSpecialSuccessDialog} from "./combat/Tor2eSpecialSuccessDialog.js";
import {Tor2eStance} from "./combat/Tor2eStance.js";

/**
 * Extend the base Actor entity.
 * @extends {Actor}
 */
export class Tor2eActor extends Actor {

    static CHARACTER = "character";
    static ADVERSARY = "adversary";
    static NPC = "npc";
    static LORE = "lore";
    static COMMUNITY = "community";

    /**
     * Return THE target of the User or undefined if none or several
     * @returns {undefined|*}
     * @private
     */
    _getTarget() {
        if (game.user.targets && game.user.targets.size === 1) {
            //should be changed  one day with multiple targets ?
            for (let target of game.user.targets) {
                return target;
            }
        }
        return undefined;
    }

    isRanger() {
        return this?.data?.data?.biography?.culture?.value?.toLowerCase()?.includes("ranger");
    }

    getActorDamageBonus(weaponUsed) {
        let actorBonusDamage;
        if (weaponUsed.data.data.ranged.value) {
            actorBonusDamage = this.data.extendedData.getRangeBonusDamage();
        } else {
            actorBonusDamage = this.data.extendedData.getCloseBonusDamage();
        }
        return actorBonusDamage;
    }

    getHeavyBlowBonus() {
        return this?.data?.data?.bonuses?.combat?.heavyBlow ?? 0
    }

    getInjury(weapon) {
        return weapon.data.data.injury.value + (this?.data?.data?.bonuses?.combat?.injury ?? 0);
    }

    getEdge(specialSuccesses, weaponUsed) {
        const baseEdge = this?.data?.data?.bonuses?.combat?.piercingBlow ?? 10;
        const nbPiercingBlow = specialSuccesses?.specialSuccessesToApply?.filter(result => result === "piercing-blow")?.length ?? 0;
        const piercingBlowBonus = this.data.extendedData.calculatePiercingBlowBonus(weaponUsed);
        const piercingBlowSpecialSuccessModifier = nbPiercingBlow * piercingBlowBonus;
        const totalEdge = baseEdge - piercingBlowSpecialSuccessModifier;
        return {
            base: baseEdge,
            piercingBlowBonus: piercingBlowBonus,
            piercingBlowSpecialSuccessModifier: piercingBlowSpecialSuccessModifier,
            total: totalEdge
        }
    }

    isWounded() {
        return this.findStatusEffectById(StatusEffects.WOUNDED) ?? false;
    }

    hasFragileHealth() {
        if (this.data.extendedData.isAdversary) {
            return this.data.data.might.value === 1;
        }

        let currentPoisonedStatusEffect = this.findStatusEffectById(StatusEffects.POISONED);

        return !!(currentPoisonedStatusEffect || this.isWounded());
    }

    async applyAllEffects(statusEffects) {
        return await this.addStatusEffectsBy(statusEffects);
    }

    isNotOOC() {
        return this.findStatusEffectById(StatusEffects.DEAD) === undefined;
    }

    getWeary() {
        return this.findStatusEffectById(StatusEffects.WEARY) !== undefined;
    }

    getMiserable() {
        return this.findStatusEffectById(StatusEffects.MISERABLE) !== undefined;
    }

    toggleItemActiveEffect(itemId, status) {
        const effects = this.getEmbeddedCollection("ActiveEffect").contents;
        const relevantEffect = effects.filter(effect => effect.data.origin.endsWith(itemId));
        if (relevantEffect.length === 1) {
            const effect = relevantEffect[0];
            effect.update({disabled: !status})
        }
    }

    /* -------------------------------------------- */
    async addStatusEffectsBy(statusEffects, options = {renderSheet: false, unique: true}) {
        const statusEffectsToBeAdded = statusEffects
            .filter(statusEffect => !this.hasStatusEffectById(statusEffect) && options.unique === true)

        await this.addStatusEffects(statusEffectsToBeAdded, options);
    }

    /* -------------------------------------------- */
    async addStatusEffectById(id, options = {renderSheet: false, unique: true}) {
        if (this.hasStatusEffectById(id) && options.unique === true) {
            return;
        }
        const statusEffect = CONFIG.statusEffects.find(it => it.id === id);
        await this.addStatusEffect(statusEffect, options);
    }

    /* -------------------------------------------- */
    async addStatusEffects(statusEffects, options = {renderSheet: false, overlay: false}) {
        let effects = statusEffects.map(effect => {
                const result = duplicate(effect);
                result["flags.core.overlay"] = effect.id === "dead";
                result["flags.core.statusId"] = effect.id;

                if (effect.id === "wounded") {
                    this.update({"data.stateOfHealth.wounded.value": effect.duration.value});
                }
                return result;
            }
        )
        await this.createEmbeddedDocuments("ActiveEffect", effects);
    }

    /* -------------------------------------------- */
    async addStatusEffect(statusEffect, options = {renderSheet: false, overlay: false}) {
        await this.deleteStatusEffectById(statusEffect.id, options);
        const effect = duplicate(statusEffect);

        this.createEmbeddedDocuments("ActiveEffect", [{
            "flags.core.statusId": effect.id,
            "flags.core.overlay": options.overlay,
            label: game.i18n.localize(effect.label),
            icon: effect.icon,
            origin: this.uuid,
        }]);
    }

    /* -------------------------------------------- */
    async toggleStatusEffectById(id, options = {renderSheet: true}) {
        const effect = this.findStatusEffectById(id);
        if (effect) {
            await this.deleteStatusEffectById(id);
        } else {
            await this.addStatusEffectById(id, options)
        }
    }

    /* -------------------------------------------- */
    hasStatusEffectById(id) {
        const effects = this.findStatusEffectById(id);
        return (effects !== undefined);
    }

    /* -------------------------------------------- */
    buildStatusEffectById(id, options = {renderSheet: true}) {
        const buildEffect = this.findStatusEffectById(id);
        const effectInConfiguration = Array.from(StatusEffects.allStatusEffects?.values())
            .find(
                it => it.id === id
            );
        return {
            value: (buildEffect !== undefined),
            icon: effectInConfiguration.icon,
            label: effectInConfiguration.label,
            duration: this.data?.extendedData?.isCharacter ? effectInConfiguration.duration : undefined
        }
    }

    /* -------------------------------------------- */
    findStatusEffectById(id) {
        return Array.from(this.effects?.values())
            .find(it => it.data.flags.core?.statusId === id);
    }

    /* -------------------------------------------- */
    async deleteStatusEffectById(id, options = {renderSheet: true}) {
        const effects = Array.from(this.effects?.values())
            .filter(it => it.data.flags.core?.statusId === id);
        await this._deleteStatusEffects(effects, options);
    }

    /* -------------------------------------------- */
    async _deleteStatusEffects(effects, options) {
        await this._deleteStatusEffectsByIds(effects.map(it => it.id), options);
    }

    /* -------------------------------------------- */
    async _deleteStatusEffectsByIds(effectIds, options) {
        await this.deleteEmbeddedDocuments('ActiveEffect', effectIds, options);
    }

    /**
     *
     * @returns {Promise<unknown>}
     * @param roll
     * @param specialSuccessNumber
     * @param weapon
     * @param targetToken
     * @param shield
     */
    async _getSpecialResultsOptions(roll, specialSuccessNumber, weapon, targetToken, shield = false) {
        if (specialSuccessNumber < 1) {
            console.error(`Special Damage (tengwar(${specialSuccessNumber})) can't be 0 or less !`);
            ui.notifications.error(game.i18n.format("tor2e.combat.warn.numberOfTengwarNotCorrect", {tengwar: specialSuccessNumber}));
        }
        let weaponUsed = this.items.get(weapon.id || weapon._id);

        let actorBonusDamage = this.getActorDamageBonus(weaponUsed)
        const edge = this.getEdge([], weaponUsed);
        const attackerStrength = this?.data?.extendedData?.getStrength();
        const defenderStrength = targetToken?.actor?.data?.extendedData?.getStrength();
        let specialSucces = this.data.extendedData.getSpecialSuccess(weapon, shield, actorBonusDamage, edge.piercingBlowBonus, edge.base, attackerStrength, defenderStrength);

        let specialSuccesses = [];
        for (let i = 0; i < specialSuccessNumber; i++) {
            specialSuccesses.push(specialSucces);
        }

        return Tor2eSpecialSuccessDialog.create({
            specialSuccessNumber: specialSuccessNumber,
            specialSuccessImg: {
                icon: "systems/tor2e/assets/images/tengwar.png",
                title: "tor2e.dice.tengwar",
                alt: "tor2e.dice.standard.6"
            },
            featDie: {
                icon: roll.getCustomLabel()[0],
                title: "tor2e.dice.feat.description",
                alt: "tor2e.dice.feat.description"
            },
            specialSuccesses: specialSuccesses,
        });
    }

    /* -------------------------------------------- */
    async attackOpponentWith(weaponName, options = {automaticDifficultyRoll: false}) {
        let targetToken = this._getTarget();
        if (targetToken === undefined) {
            ui.notifications.warn(game.i18n.localize("tor2e.combat.warn.noValidTarget"));
            return;
        }

        let weaponUsed = this.data.extendedData.getWeaponUsed(weaponName);
        if (weaponUsed === undefined) {
            ui.notifications.warn(
                game.i18n.format("tor2e.combat.warn.noValidWeaponEquipped", {skill: weaponName})
            );
            return;
        }

        let tn = this.data.extendedData._computeTN(targetToken);
        if (tn == null) {
            return;
        }

        const combatant = game.combat.combatants.find(c => c.data.actorId === this.id)
        if (!combatant) {
            ui.notifications.warn(
                game.i18n.format("tor2e.combat.warn.combatantDoesntExistInCombat", {id: this.id})
            );
            return;
        }

        let bonusPenaltyDiceAdded
        const combatData = combatant.getCombatData()
        const stance = Tor2eStance.from(combatData.stance.class)
        if (combatData.isCharacter) {
            bonusPenaltyDiceAdded = stance.getAttackBonus({actorId: this.id, engagedWith: combatData.engagedWith})
        } else {
            const targetCombatant = game.combat.getCombatantByTokenId(targetToken.id);
            const targetCombatData = targetCombatant.getCombatData();
            const targetStance = Tor2eStance.from(targetCombatData.stance.class)
            bonusPenaltyDiceAdded = targetStance.getDefenseBonus({})
        }

        let item = this.data.extendedData.getItemFrom(weaponUsed.name, weaponUsed.type);
        let shadowServant = this.data.extendedData.isHostile;
        let actionValue = item.value;
        let modifier = item.modifier;
        let isFavoured = item.isFavoured;
        let optionData = {
            weapon: weaponUsed,
            hostile: shadowServant,
            target: {
                id: targetToken.id,
                name: targetToken.name,
            },
            difficulty: tn,
        }
        let roll = await Dice.taskCheck({
            actor: this,
            target: targetToken,
            user: game.user,
            difficulty: tn,
            askForOptions: !options.automaticDifficultyRoll,
            actionValue: actionValue,
            actionName: weaponName,
            wearyRoll: this.getWeary(),
            modifier: modifier,
            shadowServant: shadowServant,
            hopePoint: tor2eUtilities.utilities.try(() => this.data.data.resources.hope.value, 0),
            favouredRoll: isFavoured,
            illFavouredRoll: this.getMiserable(),
            bonusPenaltyDiceAdded: Math.abs(bonusPenaltyDiceAdded) > 3 ? 0 : bonusPenaltyDiceAdded,
            penaltyDiceAdded: Math.abs(bonusPenaltyDiceAdded) <= 3 ? 0 : bonusPenaltyDiceAdded,
        });

        if (!roll || roll.isFailure()) {
            return;
        }

        let specialSuccessOptions;

        /* handle special damage here */
        let specialResults = roll.rollNbOfTengwarRunes();
        if (specialResults > 0) {
            let shieldUsed = this.data.extendedData.getShieldUsed();
            //Afficher la fenêtre de consommation des Tengwars
            specialSuccessOptions = await this._getSpecialResultsOptions(roll, specialResults, weaponUsed, targetToken, shieldUsed);

            if (specialSuccessOptions.cancelled) {
                return;
            }
        }

        optionData.specialSuccesses = specialSuccessOptions;
        // Continuer la suite du traitement
        await this.createAttackResultChatMessage(roll, optionData);
    }

    _isTwoHanded(weapon) {
        return weapon.data.data.twoHandWeapon.value;
    }

    _computeDamage(weaponUsed, actorBonusDamage, heavyBlows) {
        const weaponDamage = weaponUsed.data.data.damage.value;
        const isTwoHanded = this._isTwoHanded(weaponUsed);
        const bonusDamage = heavyBlows * ((isTwoHanded ? actorBonusDamage + 1 : actorBonusDamage) + this.getHeavyBlowBonus());
        const totalDamage = weaponDamage + bonusDamage;
        return {
            weapon: weaponDamage,
            bonus: bonusDamage,
            total: totalDamage,
            extra: heavyBlows > 0,
        };
    }

    _buildSpecialDamageMessage(specialSuccessesToApply) {
        return specialSuccessesToApply?.map(s => game.i18n.localize(`tor2e.combat.special-success-action.${s}.title`),)
            ?.join() ?? "-";
    }

    async createAttackResultChatMessage(roll, options = {}) {
        //create chat message for opponent to ake damage, roll protection, ...
        let weaponUsed = this.items.get(options.weapon.id || options.weapon._id);
        let actorBonusDamage = this.getActorDamageBonus(weaponUsed)
        const specialSuccessesToApply = options?.specialSuccesses?.specialSuccessesToApply;
        const heavyBlows = specialSuccessesToApply?.filter(success => success === "heavy-blow")?.length ?? 0;
        let damages = this._computeDamage(weaponUsed, actorBonusDamage, heavyBlows);
        let featDieResult = roll.getFeatDieResult(options);

        const edge = this.getEdge(options.specialSuccesses, weaponUsed)?.total ?? 10;
        let piercingShot = featDieResult >= edge;

        let chatData = {
            user: game.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        let weaponDamageDetailMessage = game.i18n.format("tor2e.combat.chat.details.damage.weapon",
            {weaponDamage: damages.weapon,});
        let bonusDamageDetailMessage = game.i18n.format("tor2e.combat.chat.details.damage.bonus",
            {bonusDamage: damages.bonus,});
        let piercingShotDescriptionMessage = piercingShot ? game.i18n.format("tor2e.combat.chat.piercing.description",
            {
                targetName: options.target.name,
                edge: edge,
                weaponInjuryValue: this.getInjury(weaponUsed),
            }) : "";

        const resultDescriptionMessage = game.i18n.format("tor2e.combat.chat.result.paragraph",
            {
                attackerName: this.name,
                weaponName: weaponUsed.data.name,
                targetName: options.target.name,
                totalDamage: damages.total,
                specialSuccesses: specialSuccessesToApply,
                hint: game.i18n.localize("tor2e.combat.chat.result.help")
            });

        let cardData = {
            owner: options.target.id,
            roll: roll,
            difficulty: options.difficulty,
            specialDamage: this._buildSpecialDamageMessage(specialSuccessesToApply),
            attacker: {name: this.name},
            target: {
                name: options.target.name,
                id: options.target.id
            },
            weapon: options.weapon,
            piercingShot: piercingShot,
            damages: damages,
            state: {
                ooc: false,
                wearyApplied: false,
                effects: false,
                damageDealt: false,
                protectionRolled: false,
            },
            resultDescription: resultDescriptionMessage,
            weaponDamageDetail: weaponDamageDetailMessage,
            bonusDamageDetail: bonusDamageDetailMessage,
            piercingShotDescription: piercingShotDescriptionMessage,
        };

        chatData = mergeObject(chatData, {
            content: await renderTemplate("systems/tor2e/templates/chat/combat-damage-card.hbs", cardData),
            flags: Tor2eChatMessage.buildExtendedDataWith(cardData),
            roll: true,
        });

        await ChatMessage.create(chatData);
    }

    async _warnLoreMasterIfNeccessary(shouldBeWeary, shouldBeMisearble) {
        if (!game.user.isGM) {
            return;
        }
        let warnWeary = !this.getWeary() && shouldBeWeary;
        let warnMisearble = !this.getMiserable() && shouldBeMisearble;
        if (warnWeary || warnMisearble) {
            await this._createWarnMessage(this, warnWeary, warnMisearble);
        }

    }

    async _createWarnMessage(actor, warnWeary, warnMisearble) {
        let chatData = {
            user: game.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        let message;
        if (warnWeary && warnMisearble) {
            message = game.i18n.format("tor2e.actors.chat.warn.wearyAndMiserable", {name: actor.name});
        } else if (warnWeary) {
            message = game.i18n.format("tor2e.actors.chat.warn.weary", {name: actor.name});
        } else {
            message = game.i18n.format("tor2e.actors.chat.warn.miserable", {name: actor.name});
        }

        let cardData = {
            ...actor,
            owner: {
                id: actor.id,
                img: actor.img,
                name: actor.name
            },
            warn: {
                message: message,
            }
        };

        let flagData = {
            warn: {
                actorId: actor.id,
                weary: warnWeary,
                miserable: warnMisearble,
                message: message
            }
        };

        chatData = mergeObject(chatData, {
            content: await renderTemplate(`${CONFIG.tor2e.properties.rootpath}/templates/chat/actions/warn-lm-card.hbs`, cardData),
            flags: Tor2eChatMessage.buildExtendedDataWith(flagData),
            roll: true,
        });

        return ChatMessage.create(chatData);
    }

    _shouldWarnPlayer(badStat, goodStat, state) {
        return badStat >= goodStat && !state;
    }

    shouldBeWeary() {
        let items = Array.from(this.items.values());
        let gearLoad = tor2eUtilities.filtering.getLoad(items);
        let currentLoad = parseInt(this.data.data.resources.travelLoad.value) + gearLoad;
        return this._shouldWarnPlayer(currentLoad, this.data.data.resources.endurance.value, this.findStatusEffectById(StatusEffects.WEARY));
    }

    shouldBeMiserable() {
        let currentShadow = parseInt(this.data.data.resources.shadow.shadowScars.value) + parseInt(this.data.data.resources.shadow.temporary.value);
        return this._shouldWarnPlayer(currentShadow, this.data.data.resources.hope.value, this.findStatusEffectById(StatusEffects.MISERABLE));
    }

    getIllFavoured() {
        let currentShadow = parseInt(this?.data?.data?.resources?.shadow?.shadowScars?.value ?? -1) + parseInt(this?.data?.data?.resources?.shadow?.temporary?.value ?? -1);
        return currentShadow < 0 ? false : this._shouldWarnPlayer(currentShadow, this.data.data.resources.hope.max, false);
    }

    /**
     * Augment the basic actor data with additional dynamic data.
     * @override
     */
    async prepareData() {
        super.prepareData();

        const actorData = this.data;
        let extendedData = {};

        if (actorData.type === Tor2eActor.ADVERSARY) extendedData = this._prepareAdversaryData(this);
        if (actorData.type === Tor2eActor.CHARACTER) extendedData = await this._prepareCharacterData(this);
        if (actorData.type === Tor2eActor.NPC) extendedData = this._prepareNpcData(this);
        if (actorData.type === Tor2eActor.LORE) extendedData = this._prepareLoreData(this);

        extendedData.combatProficiencies = new Map(Object.entries(actorData?.data?.combatProficiencies || []));

        extendedData.state = {
            weary: this.buildStatusEffectById(StatusEffects.WEARY),
            miserable: this.buildStatusEffectById(StatusEffects.MISERABLE),
            poisoned: this.buildStatusEffectById(StatusEffects.POISONED),
            wounded: this.buildStatusEffectById(StatusEffects.WOUNDED),
        }

        this.data.extendedData = extendedData;
    }

    async prepareDerivedData() {
        await super.prepareDerivedData();

        const actorData = this.data;

        if (actorData.type !== Tor2eActor.CHARACTER || !game.settings.get("tor2e", "warnLoreMasterForCharacterState")) {
            return
        }

        let actorShouldBeWeary = this.shouldBeWeary();

        let actorShouldBeMiserable = this.shouldBeMiserable();
        await this._warnLoreMasterIfNeccessary(actorShouldBeWeary, actorShouldBeMiserable)

    }

    async _prepareCharacterData(actor) {
        let constants = CONFIG.tor2e.constants;
        let actorData = actor.data;
        return {
            isCharacter: true,
            isAdversary: false,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: false,
            /**
             * Compute the TN to hit a token.
             * @param token
             * @returns {*}
             * @private
             */
            _computeTN(token) {
                let constants = CONFIG.tor2e.constants;
                let foe = game.combat.getCombatantByTokenId(token.id);
                if (!foe || !foe.actor) {
                    ui.notifications.warn(game.i18n.localize("tor2e.combat.warn.noValidCombatTarget"));
                    return;
                }
                let foeData = foe.actor.data;
                let parryBonus = foeData.extendedData.getParryBonus();
                let shield = tor2eUtilities.filtering.getItemBy(foeData.items, constants.armour, constants.shield);
                let shieldBonus = shield?.data?.data?.protection?.value || 0;
                let baseTN = this.getStrengthTn();
                return baseTN + parryBonus + shieldBonus;
            },
            getSpecialSuccess(weapon, shield, actorBonusDamage, piercingBlowBonus, edgeThreshold, strengthAttribute, adversaryAttribute) {
                return Object.values(Tor2eCharacterSpecialSuccess.from(weapon, shield, actorBonusDamage, piercingBlowBonus, edgeThreshold, strengthAttribute, adversaryAttribute))
            },
            calculatePiercingBlowBonus(weapon) {
                const group = weapon.data.data.group.value;
                const weaponGroups = CONFIG.tor2e.constants.weaponGroups;
                if (group === weaponGroups.swords) {
                    return 1;
                } else if (group === weaponGroups.bows) {
                    return 2;
                } else if (group === weaponGroups.spears) {
                    return 3;
                } else {
                    return 0
                }
            },
            getCombatProficiencies() {
                let combatProficiencies = new Map(Object.entries(actorData.data.combatProficiencies)
                    .filter(([k, v]) => k !== "brawling"));

                const brawlingValue = Math.max(...[...combatProficiencies.values()].map(cp => cp.value)) - 1;
                combatProficiencies
                    .set("brawling", {
                        icon: "systems/tor2e/assets/images/icons/weapons/dagger.png",
                        label: "tor2e.combatProficiencies.brawling",
                        roll: {associatedAttribute: "strength"},
                        type: "Number",
                        value: brawlingValue < 0 ? 0 : brawlingValue,
                        inactive: true,
                    });
                return combatProficiencies;
            },
            getTn(attribute) {
                let tns = {
                    strength: actorData.extendedData.getStrengthTn(),
                    heart: actorData.extendedData.getHeartTn(),
                    wits: actorData.extendedData.getWitsTn(),
                }
                return tns[attribute];
            },
            getStrength() {
                return actorData.data.attributes.strength.value
            },
            getHeart() {
                return actorData.data.attributes.heart.value
            },
            getWits() {
                return actorData.data.attributes.wits.value
            },
            getStrengthTn() {
                let baseTN = game.settings.get("tor2e", "tnBaseValue") || 20
                return baseTN - this.getStrength() + (actorData?.data?.bonuses?.tn?.strength ?? 0);
            },
            getHeartTn() {
                let baseTN = game.settings.get("tor2e", "tnBaseValue") || 20
                return baseTN - this.getHeart() + (actorData?.data?.bonuses?.tn?.heart ?? 0);
            },
            getWitsTn() {
                let baseTN = game.settings.get("tor2e", "tnBaseValue") || 20
                return baseTN - this.getWits() + (actorData?.data?.bonuses?.tn?.wits ?? 0);
            },
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 0
                } else {
                    return 1000;
                }
            },
            getInitiativeBonus() {
                return actor.data.data.commonSkills.battle.value;
            },
            getAttackTn(attacker, target) {
                return attacker.getStrengthTn();
            },
            _getLoad() {
                return tor2eUtilities.filtering.getLoad(actorData.items);
            },
            getload() {
                return actorData.data.resources.travelLoad.value + this._getLoad();
            },
            getEndurance() {
                return actorData.data.resources.endurance.value;
            },
            async updateEndurance(value) {
                let newEndurance = value < 0 ? 0 : value;
                return await actor.update({"data.resources.endurance.value": newEndurance});
            },
            getParryBonus() {
                return actorData?.data?.combatAttributes?.parry?.value || 0;
            },
            getProtectionRollModifier() {
                return actorData?.data?.bonuses?.defense?.protection || 0;
            },
            getHeadGearProtectionValue() {
                let allEquipedItems = tor2eUtilities.filtering.getAllEquipedItems(actorData.items, true);
                let result = tor2eUtilities.filtering
                    .getItemIn(allEquipedItems, constants.armour, [constants.headgear]);
                return (result && result.data.data.protection.value) || 0;
            },
            getArmourProtectionValue() {
                let allEquipedItems = tor2eUtilities.filtering.getAllEquipedItems(actorData.items, true);
                let result = tor2eUtilities.filtering
                    .getItemIn(allEquipedItems, constants.armour, [constants.mailArmour, constants.leatherArmour]);
                return (result && result.data.data.protection.value) || 0;
            },
            canSpendHopePoint() {
                return actorData.data.resources.hope.value;
            },
            getWeaponUsed(weaponName) {
                let allEquipedItems = tor2eUtilities.filtering.getAllEquipedItems(actorData.items);
                return tor2eUtilities.filtering
                    .getItemsBy(allEquipedItems, constants.weapon)
                    .find(weapon => weapon.name === weaponName);
            },
            getShieldUsed() {
                let allEquipedItems = tor2eUtilities.filtering.getAllEquipedItems(actorData.items);
                const shields = tor2eUtilities.filtering
                    .getItemsBy(allEquipedItems, constants.armour, constants.shield);
                return shields.length >= 1 ? shields[0] : undefined;
            },
            getRangeBonusDamage() {
                return actor.data.extendedData.getStrength();
            },
            getCloseBonusDamage() {
                return actor.data.extendedData.getStrength();
            },
            getHopePoint() {
                return actorData.data.resources.hope.value;
            },
            getItemFrom(_name, _type) {
                function _getSkillFromItemSkill(skillName) {
                    // We get the skill from the combat proficencies
                    let skill = actor?.data?.extendedData?.getCombatProficiencies()?.get(skillName.toLowerCase());

                    if (!skill && game.settings.get("tor2e", "extendedWeaponSelection")) {
                        // We get the skill from the skill name
                        skill = actor.items
                            .find(i =>
                                i.name === skillName && i.type === "skill");
                    }

                    return skill;
                }

                if (!actor) {
                    ui.notifications.warn(
                        game.i18n.format("tor2e.combat.warn.noActorFound", {name: _name})
                    );
                    return null
                }

                // We get the weapon from the actor
                let _item = actor.items
                    .find(i =>
                        i.name === _name && i.type === _type);
                if (!_item) return _item;
                let itemData = _item.data.data;
                // We get the skill name from the Weapon Group
                let skillName = itemData.group.value;
                let _weaponSkill = _getSkillFromItemSkill(skillName);
                let localizeSkillName = game.i18n.localize(itemData.group.label)
                // if no skill is found we search for skill name store in the weapon and the option for extendedWeaponSelection is true
                if ((!_weaponSkill) && game.settings.get("tor2e", "extendedWeaponSelection") === true) {
                    // We get the skill name store in the weapon
                    if (!itemData?.skill) {
                        ui.notifications.warn(
                            game.i18n.format("tor2e.combat.warn.noSkillNameAvailableWithExtendedWeaponSelection", {
                                actorName: actor.name,
                                name: _name
                            })
                        );
                        return;
                    }
                    skillName = itemData.skill.name;
                    _weaponSkill = _getSkillFromItemSkill(skillName);
                    if (!_weaponSkill) {
                        ui.notifications.warn(
                            game.i18n.format("tor2e.combat.warn.noSkillNameFoundWithExtendedWeaponSelection", {
                                actorName: actor.name,
                                name: _name
                            })
                        );
                        return;
                    }
                }

                if (!_weaponSkill) {
                    ui.notifications.warn(
                        game.i18n.format("tor2e.combat.warn.noCombatProficiencyFound", {
                            actorName: actor.name,
                            name: localizeSkillName,
                        })
                    );
                    return;
                }

                _item.value = _weaponSkill?.value || _weaponSkill?.data?.data?.value || 0;
                _item.modifier = 0;
                return _item;
            },
        }
    }

    _prepareNpcData(actor) {
        return {
            isCharacter: false,
            isAdversary: false,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: true,
            canSpendHopePoint() {
                return false;
            },
            getAttributeLevel() {
                return actor.data.data.attributeLevel.value;
            },
            getTn(attribute) {
                let baseTN = game.settings.get("tor2e", "tnBaseValue") || 20
                return baseTN - this.getAttributeLevel();
            },
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 0
                } else {
                    return 1000;
                }
            },
            getInitiativeBonus() {
                return actor.data.data.attributeLevel.value;
            },
            getParryBonus() {
                return 0;
            },
            getItemFrom(_name, _type) {
                let _item = actor ? actor.items
                    .find(i =>
                        i.name === _name && i.type === _type) : null;

                if (!_item) {
                    ui.notifications.error(game.i18n.format("tor2e.combat.error.itemNotFound"),
                        {
                            name: _name,
                            type: "_type"
                        })
                    return _item;
                }

                let itemData = _item.data.data;
                if (!itemData.skill) {
                    ui.notifications.warn(game.i18n.format("tor2e.combat.warn.incompleteItem"),
                        {
                            item: _name,
                            element: "skill"
                        })
                    return _item;
                }

                _item.value = itemData.skill.value;
                _item.isFavoured = itemData.skill.favoured.value;
                return _item;
            }
        }
    }

    _prepareLoreData(actor) {
        return {
            isCharacter: false,
            isAdversary: false,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: true,
            canSpendHopePoint() {
                return false;
            },
            getParryBonus() {
                return 0;
            },
        }
    }

    _prepareAdversaryData(actor) {
        let constants = CONFIG.tor2e.constants;
        let actorData = actor.data;
        return {
            isCharacter: false,
            isAdversary: true,
            isFriendly: false,
            isHostile: true,
            isRenownCharacter: false,
            /**
             * Compute the TN to hit a token.
             * @param token
             * @returns {*}
             * @private
             */
            _computeTN(token) {
                let constants = CONFIG.tor2e.constants;
                let foe = game.combat.getActiveCombatants().find(c => c.token.id === token.id);
                if (!foe || !foe.actor) {
                    ui.notifications.warn(game.i18n.localize("tor2e.combat.warn.noValidCombatTarget"));
                    return;
                }
                let foeData = foe.actor.data;
                let baseTN = foeData.extendedData.getParryBonus();
                let allEquipedItems = tor2eUtilities.filtering.getAllEquipedItems(foeData.items);
                let shield = tor2eUtilities.filtering.getItemBy(allEquipedItems, constants.armour, constants.shield);
                let shieldBonus = shield?.data?.data?.protection?.value || 0;
                return baseTN + shieldBonus;
            },
            getSpecialSuccess(weapon, shield, actorBonusDamage, piercingBlowBonus, edgeThreshold, strengthAttribute, adversaryAttribute) {
                return Object.values(Tor2eAdversarySpecialSuccess.from(weapon, shield, actorBonusDamage, piercingBlowBonus, edgeThreshold))
            },
            calculatePiercingBlowBonus(weapon) {
                return 2;
            },
            getTn(attribute) {
                let baseTN = game.settings.get("tor2e", "tnBaseValue") || 20
                return baseTN - this.getAttributeLevel();
            },
            canSpendHopePoint() {
                return false;
            },
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 1000;
                } else {
                    return 0
                }
            },
            getInitiativeBonus() {
                return actor.data.data.attributeLevel.value;
            },
            getAttackTn(attacker, target) {
                return target.getStance()?.difficulty;
            },
            /**
             * convenient function to call attribute but using the word Strength.
             * @returns {*}
             */
            getStrength() {
                return this.getAttributeLevel();
            },
            getAttributeLevel() {
                return actorData.data.attributeLevel.value;
            },
            getEndurance() {
                return actorData.data.endurance.value;
            },
            async updateEndurance(value) {
                let newEndurance = value < 0 ? 0 : value;
                return await actor.update({"data.endurance.value": newEndurance});
            },
            getArmourProtectionValue() {
                let result = tor2eUtilities.filtering
                    .getItemIn(actorData.items, constants.armour, [constants.mailArmour, constants.leatherArmour]);
                return (result && result.data.data.protection.value) || 0;
            },
            getParryBonus() {
                return actorData?.data?.parry?.value || 0;
            },
            getHeadGearProtectionValue() {
                let result = tor2eUtilities.filtering
                    .getItemIn(actorData.items, constants.armour, [constants.headgear]);
                return (result && result.data.data.protection.value) || 0;
            },
            getProtectionRollModifier() {
                return actorData?.data?.bonuses?.defense?.protection || 0;
            },
            getWeaponUsed(weaponName) {
                let constants = CONFIG.tor2e.constants;
                return tor2eUtilities.filtering
                    .getItemsBy(actorData.items, constants.weapon)
                    .find(weapon => weapon.name === weaponName);
            },
            getShieldUsed() {
                let constants = CONFIG.tor2e.constants;
                const shields = tor2eUtilities.filtering
                    .getItemsBy(actorData.items, constants.armour, constants.shield);
                return shields.length >= 1 ? shields[0] : undefined;
            },
            getRangeBonusDamage() {
                return actor.data.extendedData.getAttributeLevel(actorData);
            },
            getCloseBonusDamage() {
                return actor.data.extendedData.getAttributeLevel(actorData);
            },
            getHopePoint() {
                return 0;
            },
            getItemFrom(_name, _type) {
                let _item = actor ? actor.items
                    .find(i =>
                        i.name === _name && i.type === _type) : null;

                if (!_item) {
                    ui.notifications.error(game.i18n.format("tor2e.combat.error.itemNotFound"),
                        {
                            name: _name,
                            type: "_type"
                        })
                    return _item;
                }

                let itemData = _item.data.data;
                if (!itemData.skill) {
                    ui.notifications.warn(game.i18n.format("tor2e.combat.warn.incompleteItem"),
                        {
                            item: _name,
                            element: "skill"
                        })
                    return _item;
                }

                _item.value = itemData.skill.value;
                _item.isFavoured = itemData.skill.favoured.value;
                return _item;
            }

        }
    }
}