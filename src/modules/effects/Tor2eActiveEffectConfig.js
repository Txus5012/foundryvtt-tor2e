import {tor2e} from "../config.js";

export default class Tor2eActiveEffectConfig extends ActiveEffectConfig {
    get template() {
        return "systems/tor2e/templates/effects/activeEffect-config.hbs";
    }

    /** @override */
    getData(options) {
        let sheetData = super.getData(options);
        sheetData.isRaw = game.settings.get("tor2e", "useRawModeForActiveEffect");
        sheetData.config = tor2e;
        return sheetData;
    }
}