# The One Ring 2nd ed. System

## Disclaimer

An UNOFFICIAL system for playing "The One Ring" second Ed. from Free League on Foundry VTT.

This system only provides support for character, creature, items, system, talent and character sheets.

There is no game content or images in this system.

Should you wish to populate the system with game content for your own use please purchase the rules.

My intention is to enable people to play The One Ring RPG remotely and not to create a fully enabled/automated system.

"Trademarked and copyrighted content like 'Hobbit', 'Sauron', 'Gandald', placenames, historical names, and others,
belong to the license owners, and are used as is under Fair Use or Fair Dealing with no intent of commercial
distribution."

## Reminder

This system is a fork from The One Ring 1st Edition system (https://gitlab.com/herve.darritchon/foundryvtt-tor2e). As
both TTRPG rules are very close, it was more simple to initialize this from Tor1E system. But be aware, I need to adapt some of
the features to be compliant with **The One Ring Second Edition Core Rules** from Free League Publishing.

Migration from v1 to v2 should be finished.

## Installation

You can install the TOR2e Rule System using
this [system.json file URL](https://gitlab.com/herve.darritchon/foundryvtt-tor2e/-/raw/main/src/system.json)

## Overview of the system

### Roadmap

* Currently phase : RELEASE

#### Release phase

The system will not be finished but all the core features will be available for you. But the structure of the Actors and
Items should be stable. Next structure evolution will be covered by ascendant compatibility and migration script.

## Features

### Releases

Follow this [link](https://gitlab.com/herve.darritchon/foundryvtt-tor2e/-/releases) to read all the new features from the releases.

### Sheets

**UI/UX rule**

* In all the sheets, you have this king of UI/UX rule. You should ALT + Click to modify the content of the sheet (add a
  level to a skill, remove an item with the bin, add a health status such as weary, ...).
* The data displayed in the circles follow the rule
    * beige background : editable
    * grey background : not editable
* Weapon in Actor sheets are drag'n droppable to macro bar (shortcut bar in the bottom of the screen). This way you can
  create easily some macro to have a convenient way to attack without opening the sheet.
* If you click on a element that should roll some dice, you can SHIFT + CLICK to avoid the customization roll popup and
  accept the value by default.

#### Character

This sheet used to store, display and manage the pc data.

![Biography, Attributes and Common Skills](https://gitlab.com/herve.darritchon/foundryvtt-tor2e/uploads/43f786786a6c353f49ed0d72cd6a5a72/Capture_d_écran_2021-08-02_à_09.58.32.png)
![Traits with Specialities and Distinctives Features, Virtues and Valors, Resources (endurance, fatigue, hope and Shadow) and Heatlh (weary, wounded, poisoned, miserable, ...)](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/16f8f59dd11024921ce6e87cae114af4/Capture_d_écran_2021-07-02_à_09.31.33.png)
![Combat and Gears](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/961cb378dff536241272a23ae9b90a47/Capture_d_écran_2021-07-02_à_09.31.49.png)
![Miscellaneous data](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/e1e677bab61afa9a92a364046ebe354d/Capture_d_écran_2021-07-02_à_09.32.06.png)

With this sheet, you can store all the data needed by your character (attributes, skills, traits, specialities, items,
combat skills, resources (endurance, hope, shadow, ...), Health, ... but you are able also to track all the experience,
the history, the background and the adventures.

You can **ALT click** a skill name to favoured it, you can click on diamonds with ALT to add one level to a skill or **
SHIFT+ALT** to remove one level to it.

You can roll dice from the sheet by clicking on a skill name, if you are in combat you can roll an attack by clicking on
the weapon name, you can roll a protection roll by clicking on the Armour label in the combat block and even Fear or
Corruption using the Virtue and Valor label.

Some of the data are automatically updated like the Fatigue value using all the gears you have and you have some hints (
red background) in the sheet if you should be weary (endurance < fatigue). Same feature for the Shadow value with Hope
instead of the Fatigue. The fatigue value is computed from all the gears equipped and their load.

Some of the combat value (shield, armour and headgear) are used to compute automatically the defence of the character if
he is attacked. You should have equipped the gear to be able to see it in the combat block and used during combat.

#### Non Player Character

This sheet is used for non-combatant NPC, you have a digest of this character to use in your game. You can add or remove
skills, traits, ... manage its health, endurance and obviously make roll test from the sheet by clicking on the skill
names.

![A NPC Sheet example](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/0235cbf493925aa9067c9fc6c09cfdbc/Capture_d_écran_2021-07-02_à_10.49.47.png)

You have a description block to store special information about the actor.

For special non-combatant Adversary, NPC that can be hostile. You have a way to change the NPC from friendly (by
default) to default. This way, the NPC will be able to follow the rule of the Shadow Servant (especially for dice (eye
of sauron and gandalf rune)).

#### Adversary

This sheet is used for Adversary or combatant NPC. as for NPC, you have a digest of all the stats needed by such actor (
stats, health, skills, gears and special abilities)

![An Adversary Sheet example](https://gitlab.com/herve.darritchon/foundryvtt-tor2e/uploads/2d97882528b523ea92562f45694adb32/Capture_d_écran_2021-08-02_à_10.02.10.png)

If some data are different from NPC, all the features for favoured skill, skill rolling test, ... are the same for
Adversary as NPC.

If an adversary is in combat, you can attack from the sheet by clicking on the weapon.

It is also possible to change hostile state of Adversary (by default) to friendly because for some combatant NPC you
need to ;)

#### Lore Character

Lore character sheet is from renown character like Gandalf, Thranduil, ... when you don't need stats. This way you
canhave a token, display an artwork and also store some data about the character in description blocks. It's a very
simple sheet but you can manage health, hostile/friendly state.

![A Lore Sheet example](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/c1d48d4601496832ed4f5efc4e9fad5f/Capture_d_écran_2021-07-02_à_11.13.14.png)

There is a special tag on the sheet, `Can be a patron`, this is used in the community sheet to be able to transform the
Lore Character to a patron for the fellowship.

#### Community

This special actor sheet is used to manage the Fellowship. This way you can have a specific token to reprensent the
felloship on a map for travelling. your players can also use this sheet to set their travel roles and make rolls during
the travel phase.

In the top part of the sheet, you can manage the fellowship membership by drag'n dropping actor (PCs) from the actor tab
to the sheet. Also you can add 2 type of Artwork. An artwork for the community itself (the banner bottom to the icon (
ring)). It is a convenient way to have an image with all the characters. In the top part you can also manage connections
and patrons for the fellowship. same as membership,you can drag'n drop from actor tab some npc/lore actors and manage if
they are connections (known) or patrons.

![The top Community Sheet example](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/fcff7eb86c15e1becdca5ccd311ecdd7/Capture_d_écran_2021-07-02_à_11.21.39.png)

The bottom part is used for travelling, players can set their role in the travel process and depending on their role
they will be able to roll some specifc test. It is a convenient way to manage travelling and avoiding lots of
boilerplate macros/clicks/... You can move the actor between avaiable members blocks (no role set) to a specific role (
guuide, hunter, scouts,lookouts). Only one Guide is possible.

![The bottom Community Sheet example](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/e46335bf998466a28436cfad5cffa88d/Capture_d_écran_2021-07-02_à_11.21.56.png)

### Items

there is a lot of item types available and used by the system and some of them have subtype :

* Traits (specialities & distinctive features)
* fell-ability for adversaries
* Skill (all type of skills including combat)
* Armour (all armour gears such as armour, shield, headgear)
* Weapon (all type of weapon even natural such as claws)
* Miscellaneous (for non combat gears like music instrument)
* Reward (qualities or cultural) from valour
* Virtues (masteries or cultural) from Wisdom

![Item list](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/f3b36d13a17bd4257d28a8e02d8ec588/Capture_d_écran_2021-07-02_à_11.32.46.png)

### Token

When you drop an actor on a scene it became a Token. On token you can modify some values directly on it using the Hud.

![Some tokens with effects and stances](https://gitlab.com/herve.darritchon/foundryvtt-tor2e/uploads/d205457851fc42a65b5ae58ad8768e91/Capture_d_e%CC%81cran_2021-08-02_a%CC%80_10.01.40.png)

You can see and change the 2 resources (hope and endurance), also you can see and manage status effects (weary, wounded,
miserable and poisoned) on the token as icon. And finally for PC in combat, you can see and manage the Stance (rearward,
defensive, open and forward).

You just need to right click on the token to display the customised and enhanced HUD with specific The One Ring Feature.

![The customize HUD](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/6c675615102a9673488f96d11715d77e/Capture_d_écran_2021-07-02_à_10.09.43.png)

### Dice and Roll

All the dice needed by the system are implemented (Feat, Success) and lots of roll are implemented (skill, combat,
protection, fear, corruption, ...). All can be done from sheets or macros.

The system is compatible with Dice So Nice, so you can have 3D dice rolling on the virtual table which is very nice !

![Some dice rolling](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/323a982231155f843cb2d9edb32eae50/Capture_d_écran_2021-07-02_à_11.43.41-dice.png)

The roll open a window to customize the roll, you can add success bonus die, feat die, keep the best or the worst and
you can set the difficulty (TN) of the roll.

![The customization roll window](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/1c5e6cef65e9a63377d09bc614a46185/Capture_d_écran_2021-07-02_à_11.44.24.png)

The roll is assessed and the result is displayed as a chat message, where you can see a digest of the roll, the result,
the formula.

![roll result chat message](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/900e9c37b09f39ccfd9f788306725f12/Capture_d_écran_2021-07-02_à_11.43.41-chat-message.png)

You can also clicking on the hand in the chat message spend a Hope point and the roll is assessed again against the same
TN and new resultis displayed. This hand is only displayed if spending a hope point is possible and if it changes the
result of the roll.

### Combat

For a better comprehension, please look at this [video of a combat](https://youtu.be/6aQFelIrR-s) with adversaries and
pcs.

Some of the combat process is automatize :

* Combat creation
* Rolling for combat and computing bonus dice
* Initiative depending on attack/defense role and pc stance
* Opening volley phase
* Combat turns with initiative and TN to land an attack computed automatically depending on the defense stats of the
  target (parry, armour gears) and the stance of the attacking actor.
* Result of the attack is processed and evaluated. Also you have a chat message to display the result and also using a
  contextual menu, you can apply automaticaly the result of the attack (damage, wound, effects (weary, out of combat,
  ...).

Make a long story short, look at the video above ;) There is a lot of features in the combat.

### Macro

#### Sheet

You and your players can drag'n drop the character sheet they own in the macro bar. It creates a macro to open the sheet in a convenient way !  

**Troubleshooting**
If your players cannot drag their actors at all, they lack the **'Create New Tokens'** permission for actors that they own. This must be enabled for the module to function correctly.

#### Weapons

You can drag'n drop weapons in the shortcut bar to create automatically attack macro and also you can drag'n drop an
actor to create a macro that open the sheet.

![the shortcut bar](https://gitlab.com/herve.darritchon/foundryvtt-tor1e/uploads/d362056376e887775c9cb1031045db07/Capture_d_écran_2021-07-02_à_12.11.51.png)

### Dice

if you want to roll from the chat some TOR Dice, you can use :

* ds (for success dice)
* dw (for weary success dice)
* df (for feat die for pc and npc)
* de (for feat die (adversary)

#### examples:

* `/r 1df + 3ds` will make a roll for a skill with 3 levels
* `/r 1df + 2dw` will make a roll for a skill with 2 levels for a weary character or npc
* `/r 1de + 1dw` will make a roll for a skill with 1 level for a weary adversary

## Footage of the system

### Alpha phase

Currently phase. Active development but as it is a fork of TOR1e which is un beta phase, **you can use this system to play !**.

### Footage

#### Tutorial

Please, take a look at Matt Kay tutorial on Foundry VTT with Tor2e system. You can follow him on the **3 skulls tavern**.
It is really interesting and I think you can learn from this video.

* [Matt Kay Tutorial](https://www.youtube.com/embed/-v0koKbM12s)

#### Other footage from myself

* [NPC Creation](https://youtu.be/519eBVcQx9o)
* [Adversary Creation](https://youtu.be/E9aiu8ChDEg)
* [Character Creation](https://youtu.be/e1GRHjjOo9M)
* [Rolling some dice](https://youtu.be/33bM3WdbtfI) with an NPC
* [Example of combat](https://youtu.be/6aQFelIrR-s) with adversaries and pcs
* [Community Sheet](https://youtu.be/Bnt-P_7iZb4) to manage your fellowship

## Supported languages

* English (default)
* French
* Spanish

## Supported Modules

* Dice so Nice to have 3D Dice rolling on the VTT.
* VTTA-Tokenizer to make token easier.
* foundryvtt-dice-cheater-protector to protect from cheaters.
* GM Screen to have like a GameMaster Screen in FVTT.

## Contribute

### Feedback & Support

If you have any suggestions or feedback, please contact me on discord (Hervé Darritchon aka Throdo Prodoufoot -
herveDarritchon#7262) or in the channel dedicated to The One Ring Rule Set 
- In english ([English Discord Server]( https://discord.me/theonering) , channel `#foundry`). 
- En français ([French Discord Server](https://discord.gg/pPSDNJk) , channel `#anneau-unique`).

### Code Style

* You must use jslint code style
* use of === is mandatory to Merge Request in js file
* never use directly a path in a string, such as "system/tor2e/templates" to avoid coupling between app structure and
  code. Instead use CONFIG.tor2e.properties.rootpath properties.
* never put html directly inside a js file. Instead use Handlebars partials
* don't modify tor2e.css directly, it is generated by less and you have to change the less files of the project.

### Image filtering for Icons

use online site : [https://www12.lunapic.com/editor/](https://www12.lunapic.com/editor/) with color : `#f44336`

## Acknowledgement

### Contributors

* `Ghorin#7155` "The Rule Master" (Discord) - For his help testing and validating and for the long feedbackshe has done.
  Please find its [gaming aids](http://ostolinde.free.fr/au_aidesdejeu.html) sadly only in french ;)
* `Milchreisrocker#2918` "The Dice Designer" (Discord) - Thanks for the dice textures and soon the bump mappings
* `XyxorSwords#9389` (Discord) - For Spanish file translation.
* `Undesconocido#0001` (Discord) - For Spanish file translation.
* `Torment#0990` (Discord) - For his help testing the tool and making feedbacks.

### Icons

Thanks to `Milchreisrocker#2918` (Discord), we got rid of the Icons from LotR Card Game and we have our own icons ! No
issue with copyright anymore !
Thanks to him for being able to use it.

## Tor2E inside docker

I use the images from felddy (https://hub.docker.com/r/felddy/foundryvtt), you can find help and documentation.

You can find a directory (`deployment`) on the gitlab root directory of the system. Inside this directory you can find
a `docker-compose` file.

With this docker-compose file, you can start your specific foundry vtt environment but to make it run you have to set
some environment variables :

* FVTT_SECRETS_FILE --> where the secret file is store with FVTT credentials to download the binary.
* FVTT_IN_DOCKER_DATA_DIRECTORY --> where the data directory inside FVTT Container is mounted.
* FVTT_IN_DOCKER_CONTAINER_CACHE_DIRECTORY --> where the cache directory used by FVTT image to retrieve and store the
  FVTT binary.
* FVTT_IN_DOCKER_TOR2E_DIRECTORY --> the source directory of the TOR2E, used during dev mode.
