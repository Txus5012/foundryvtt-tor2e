import * as Dice from "./sheets/dice.js";
import Tor2eChatMessage from "./chat/Tor2eChatMessage.js";
import {StatusEffects} from "./effects/status-effects.js";
import {Tor2eRecoverHopeDialog} from "./rest/Tor2eRecoverHopeDialog.js";

export const tor2eUtilities = {};

/**
 * Take an array of values and split it in 2 part depending on the index in the array
 * @param abilities
 * @returns {*[]}
 * @private
 */
function _splitInTwo(abilities) {
    let lefts = abilities.filter((v, i) => !(i % 2));
    let rights = abilities.filter((v, i) => i % 2);
    return [lefts, rights]
}

/**
 * lore --> Lore, hunting --> Hunting
 * @param string
 * @returns {string}
 */
function _capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Get itemId from an event by bubbling to the item class and get the id
 * @param event
 * @returns {string}
 * @private
 */
function _getActorId(event) {
    let element = event.currentTarget;
    return element.closest(".actor").dataset.actorId;
}

/**
 * Delete Actor from a list using its id
 * @returns {string}
 * @private
 * @param actor
 * @param _id
 * @param list
 * @param fromAttribute
 */
function _deleteActorFrom(actor, _id, list, fromAttribute) {
    let index = list.findIndex(function (element) {
        return element.id === _id;
    });
    if (index > -1) {
        list.splice(index, 1);
    }
    actor.update({[`${fromAttribute}`]: list})
}

/**
 * Add an actor to a List
 * @returns {string}
 * @private
 * @param actor
 * @param currentActor
 * @param list
 * @param fromAttribute
 */
function _addActorTo(actor, currentActor, list, fromAttribute) {
    list.push(currentActor);
    actor.update({[`${fromAttribute}`]: list})
}

/**
 * Get actorId from an event by bubbling to the actor class and get the id
 * @param event
 * @returns {string}
 * @private
 */
function _getItemId(event) {
    let element = event.currentTarget;
    return element.closest(".item").dataset.itemId;
}

/**
 *
 * @param element
 * @param extra
 * @returns {*}
 * @private
 */
function _getModifier(element, extra) {
    let actionBonus = parseInt(element.dataset.actionValueBonus);
    return isNaN(actionBonus) ? 0 : actionBonus
}

tor2eUtilities.combat = {
    "noActiveCombatInScene": function (_sceneId) {
        const sceneIds = Array.from(game.combats.values()).map(c => c.scene.id)
        return sceneIds.find(id => id === _sceneId) === undefined
    }
}

tor2eUtilities.macro = {
    "rollItemMacro": async function (_name, _type) {
        const speaker = ChatMessage.getSpeaker();
        let actor;
        if (speaker.token) actor = game.actors.tokens[speaker.token];
        if (!actor) actor = game.actors.get(speaker.actor);

        if (!actor) return ui.notifications.warn(game.i18n.localize("tor2e.macro.error.noActorAvailable"));

        let item = actor.data.extendedData.getItemFrom(_name, _type);

        if (!item) return ui.notifications.warn(game.i18n.format("tor2e.macro.error.itemIsMissing",
            {
                "itemName": _name,
                "itemType": _type,
                "actorName": actor.name,
            }));

        await actor.attackOpponentWith(_name);
    }
}

tor2eUtilities.filtering = {
    /**
     * Filter a list of items by its type (weapons, fellAbility, armour, ...) and its group ( for fellAbility for example, it could be Hate, Virtues, Valor)
     * This method returns left and right because it is used in a Two columns display.
     * @param items
     * @param type
     * @param subgroup
     * @returns {*[]}
     */
    "getAndSplitItemsBy": function (items, type, subgroup = null) {
        let elements = this.getItemsBy(items, type, subgroup);
        return _splitInTwo(elements);
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.filter(function (item) {
                return item.type === type;
            });
        } else {
            return items.filter(function (item) {
                return item.type === type && item.data.data.group.value === subgroup;
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsNot": function (items, type, subgroup) {
        return items.filter(function (item) {
            return item.type === type && item.data.data.group.value !== subgroup;
        });
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.find(function (item) {
                return item.type === type;
            });
        } else {
            return items.find(function (item) {
                return item.type === type && item.data.data.group.value === subgroup;
            });
        }
    },
    /**
     * Get the total load of the character
     * @param items List of all items
     * @returns {*}
     */
    "getLoad": function (items) {
        return items
            .filter(item => !(item?.data?.data?.dropped?.value ?? false))
            .filter(function (item) {
                return item?.data?.data?.load?.value > 0;
            }).map(function (item) {
                return item.data.data.load.value;
            }).reduce((a, b) => a + b, 0);
    },
    /**
     * Get all the equipped equipment
     * @param items List of all equipped items
     * @returns {*}
     */
    "getAllEquipedItems": function (items) {
        return items.filter(
            item => item.isEquipped()
        );
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * The list of elements can be filtered by a list of groups. All the elements belonging to one of the groups.
     * @param items
     * @param type
     * @param groups
     * @param equipped
     * @returns {*}
     */
    "getItemIn": function (items, type, groups = null, equipped = false) {
        return items.find(function (item) {
            if (groups === null) {
                if (equipped && item.data.data.equipped) {
                    return item.type === type && item.data.data.equipped.value === true;
                } else {
                    return item.type === type;
                }
            } else {
                if (equipped && item.data.data.equipped) {
                    return item.type === type && groups.includes(item.data.data.group.value) && item.data.data.equipped.value === true
                } else {
                    return item.type === type && groups.includes(item.data.data.group.value)
                }
            }
        });
    }
}

tor2eUtilities.utilities = {
    /**
     *
     * @param fn
     * @param fallback
     * @returns {null|*}
     * @private
     */
    try: function (fn, fallback = null) {
        try {
            return fn();
        } catch (error) {
            return fallback
        }
    }
}

tor2eUtilities.rolling = {
    /**
     * Roll a skill test.
     * @returns {*}
     * @param options
     * actorId,
     * skillName,
     * actionName,
     * automaticDifficultyRoll,
     * associateSkill
     */
    "skillRoll": async function (options = {}) {
        let actor = game.actors.get(options.actorId);
        let skill = getProperty(actor.data, `data.commonSkills.${options.skillName}`) || null;
        if (skill) {
            let difficulty = actor.data.extendedData.getTn(options.associateAttribute)
            return await Dice.taskCheck({
                actor: actor,
                user: game.user,
                actionValue: skill.value,
                difficulty: difficulty,
                actionName: options.actionName,
                askForOptions: options.automaticDifficultyRoll,
                wearyRoll: actor.getWeary(),
                modifier: 0,
                shadowServant: false,
                favouredRoll: skill.favoured.value,
                illFavouredRoll: actor.getIllFavoured(),
                miserable: actor.getMiserable(),
            });
        } else {
            ui.notifications.warn(
                game.i18n.format("tor2e.roll.warn.noSkillFound",
                    {skillName: options.skillName, actorName: actor.name})
            );
        }
    },

}

async function _recoverHopePoint(actor) {
    let communities = game.actors.filter(a => a.type === CONFIG.tor2e.constants.actors.type.community)

    if (communities.length !== 1) {
        ui.notifications.warn(game.i18n.localize("tor2e.sheet.actions.rest.dialog-box.warn.one-community_actor_expected"));
        return;
    }
    let community = communities[0];
    let hopePoints = actor.data.data.resources.hope;
    await Tor2eRecoverHopeDialog.create({
        actorId: actor.id,
        actorHopePoints: hopePoints,
        communityId: community.id,
        fellowshipPoints: community.data.data.fellowshipPoints
    });
}

tor2eUtilities.eventsProcessing = {

    async onToggleEffect(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let effectId = element.dataset.effectId;

        if (event.altKey) {
            await actor.toggleStatusEffectById(effectId)
        }
    },
    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        $(element).siblings(".editor-container").toggle();
        $(element).toggleClass("expanded");
    },

    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onEditorToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        let editorWrapper = $(element).closest('li').children('.editor-container-toggelable');
        $(editorWrapper).toggleClass("show");
        $(editorWrapper).find(".editor-content").show();
        $(editorWrapper).find(".tox-tinymce").hide();
    },

    /**
     * Edit an item when you click on the edit icon
     * using a Popup window.
     * @param event
     * @returns {*}
     */
    "onItemEdit": async function (event) {
        event.preventDefault();
        let itemId = _getItemId(event);
        let item = this.actor.items.get(itemId);
        item.sheet.render(true);
    },

    /**
     * Open an actor from the list when you click on the edit button
     * @param event
     * @returns {*}
     */
    "onActorEdit": function (event) {
        event.preventDefault();
        let actorId = _getActorId(event);
        let actor = game.actors.get(actorId)
        actor.sheet.render(true);
    },

    /**
     * Delete an actor from the list when you click on the trash
     * @param extra
     * @param event
     * @returns {*}
     */
    "onActorDelete": function (extra = {}, event) {
        event.preventDefault();
        if (event.altKey) {
            let actorId = _getActorId(event);
            return _deleteActorFrom(this.actor, actorId, extra.list, extra.attribute);
        }
    },

    /**
     * Move an inline actor to the patron zone
     * @param extra
     * @param event
     * @returns {*}
     */
    "onExchangeZone": function (extra = {}, event) {
        event.preventDefault();
        if (event.altKey) {
            let actorId = _getActorId(event);
            let currentActor = extra.from.list.find(element => element.id === actorId)
            _addActorTo(this.actor, currentActor, extra.to.list, extra.to.attribute)
            return _deleteActorFrom(this.actor, actorId, extra.from.list, extra.from.attribute);
        }
    },

    /**
     * Update the value of an attribute of an inline actor in a list.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onInlineActorEdit": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;

        let actorId = _getActorId(event);
        let list = extra.list;
        let fromAttribute = extra.attribute;


        let value;
        // if the element is a checkbox then we have to translate the state (chack) into a boolean
        if (element.type === "checkbox") {
            value = element.checked === true;
        } else {
            value = element.value;
        }

        list.find(elt => elt.id === actorId).location = value;

        return this.actor.update({[`${fromAttribute}`]: list})
    },

    /**
     * Delete an item when you click on the trash
     * @param event
     * @returns {*}
     */
    "onItemDelete": async function (event) {
        event.preventDefault();
        if (event.altKey) {
            let itemId = _getItemId(event);
            return await this.actor.deleteEmbeddedDocuments("Item", [itemId]);
        }
    },

    "ontoggleEquippedState": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let itemId = _getItemId(event);
        let item = actor.items.get(itemId);
        let primaryAttribute = element.dataset.primaryAttribute;
        let secondaryAttribute = element.dataset.secondaryAttribute;
        const itemStatus = getProperty(item.data, primaryAttribute);
        let newStatus = !itemStatus;

        if (newStatus) {
            item.update({[secondaryAttribute]: false});
        }

        actor.toggleItemActiveEffect(itemId, newStatus);
        item.update({[primaryAttribute]: newStatus});
    },

    "ontoggleDroppedState": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let itemId = _getItemId(event);
        let item = actor.items.get(itemId);
        let primaryAttribute = element.dataset.primaryAttribute;
        let secondaryAttribute = element.dataset.secondaryAttribute;
        const itemStatus = getProperty(item.data, primaryAttribute);
        let newStatus = !itemStatus;

        if (newStatus) {
            actor.toggleItemActiveEffect(itemId, false);
            item.update({[secondaryAttribute]: false});
        }

        item.update({[primaryAttribute]: newStatus});
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemName": async function (extra = {}, event) {

        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let itemId = _getItemId(event);
        let item = actor.items.get(itemId);
        let isActionFavoured = element.dataset.actionFavouredValue.toBoolean();

        if (event.altKey) {
            if (actor.data.extendedData.isCharacter && item.type === "weapon") {
                ui.notifications.warn(game.i18n.localize("tor2e.roll.warn.impossibleToFavouredAWeapon"));
                return undefined;
            }
            let field = element.dataset.actionFavouredName;
            return item.update({[field]: !isActionFavoured});
        } else {
            if ((actor.data.extendedData.isCharacter && item.type === "skill" && item.data.data.group.value === "combat")
                || (!actor.data.extendedData.isCharacter && item.type === "skill")) {
                let automaticDifficultyRoll = !event.shiftKey;
                let associateRawAttribute = element.dataset.associateAttributeName;
                return await Dice.taskCheck({
                    actor: actor,
                    user: game.user,
                    difficulty: actor.data.extendedData.getTn(associateRawAttribute),
                    actionValue: element.dataset.actionValue,
                    actionName: item.name,
                    askForOptions: automaticDifficultyRoll,
                    wearyRoll: actor.getWeary(),
                    modifier: _getModifier(element, extra),
                    shadowServant: actor.data.extendedData.isHostile,
                    hopePoint: tor2eUtilities.utilities.try(() => actor.data.data.resources.hope.value, 0),
                    favouredRoll: isActionFavoured,
                    illFavouredRoll: actor.getIllFavoured(),
                    miserable: actor.getMiserable(),
                });
            }

            let ownedItem = undefined

            if (item) ownedItem = actor.data.extendedData.getItemFrom(item.name, item.type);

            if (!ownedItem) return ui.notifications.warn(game.i18n.format("tor2e.macro.error.itemIsMissing",
                {
                    "itemName": item.name,
                    "itemType": item.type,
                    "actorName": actor.name,
                }));

            await actor.attackOpponentWith(ownedItem.data.name, {automaticDifficultyRoll: event.shiftKey});
        }
    },

    /**
     * Either update the a skill toggling the favoured state if you click with alt key.
     * Or roll against this skill if no other key is used.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillName": async function (extra = {}, event) {

        event.preventDefault();
        let element = event.currentTarget;
        let associateRawAttribute = element.dataset.associateAttributeName;
        let actor = this.actor;
        let isActionFavoured = element?.dataset?.actionFavouredValue?.toBoolean();

        if (event.altKey) {
            if (isActionFavoured !== undefined) {
                let field = element.dataset.actionFavouredName;
                return actor.update({[field]: !isActionFavoured});
            }
        } else {
            let automaticDifficultyRoll = !event.shiftKey;

            await Dice.taskCheck({
                actor: actor,
                user: game.user,
                difficulty: actor.data.extendedData.getTn(associateRawAttribute),
                actionValue: element.dataset.actionValue,
                actionName: element.dataset.actionName,
                askForOptions: automaticDifficultyRoll,
                wearyRoll: actor.getWeary(),
                modifier: _getModifier(element, extra),
                shadowServant: actor.data.extendedData.isHostile,
                hopePoint: tor2eUtilities.utilities.try(() => actor.data.data.resources.hope.value, 0),
                favouredRoll: isActionFavoured,
                illFavouredRoll: actor.getIllFavoured(),
                miserable: actor.getMiserable(),
            });
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.items.get(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ? parseInt(element.dataset.actionValue) : 0;
            return item.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.items.get(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ? parseInt(element.dataset.actionValue) : 0;
            let max = parseInt(element.dataset.actionMaxValue);
            return item.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            let max = parseInt(element.dataset.actionMaxValue);
            return this.actor.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onThreeStatesModify": function (extra = {}, event) {

        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : 2});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value + 1 <= 2) ? value + 1 : 0});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param event
     * @returns {*}
     */
    "onSkillEdit": function (event) {
        event.preventDefault();

        let element = event.currentTarget;
        let itemId = _getItemId(event);
        let item = this.actor.items.get(itemId);
        let field = element.dataset.field;

        // if the element is a checkbox then we have to translate the state (check) into a boolean
        if (element.type === "checkbox") {
            return item.update({[field]: element.checked === true});
        }
        return item.update({[field]: element.value});
    },

    "onProlongedRest": async function (extra = {}, event) {
        event.preventDefault();
        let message;
        let chatMessage = new Tor2eChatMessage();

        await _recoverHopePoint(this.actor);

        const img = {
            src: "systems/tor2e/assets/images/icons/rest/person-in-bed.svg",
            title: "tor2e.sheet.actions.rest.prolonged.title",
            alt: "tor2e.sheet.actions.rest.prolonged.label"
        }

        let strength = this.actor.data.data.attributes.strength.value;
        let maxEndurance = this.actor.data.data.resources.endurance.max;
        let currentEndurance = this.actor.data.data.resources.endurance.value;
        let restoredEndurance = this.actor.isWounded() ? currentEndurance + strength : maxEndurance;

        let enduranceAfterRest = restoredEndurance > maxEndurance ? maxEndurance : restoredEndurance;

        if (this.actor.isWounded()) {
            message = game.i18n.format("tor2e.sheet.actions.rest.prolonged.woundedRestoredMessage", {
                name: this.actor.name,
                amount: enduranceAfterRest - currentEndurance,
                endurance: enduranceAfterRest
            });
            this.actor.update({"data.resources.endurance.value": enduranceAfterRest});

            await chatMessage.createRestMessage(this.actor, message, img)
            return
        }

        if (currentEndurance === restoredEndurance) {
            message = game.i18n.format("tor2e.sheet.actions.rest.prolonged.noRestoredMessage", {
                name: this.actor.name
            });
            await chatMessage.createRestMessage(this.actor, message, img)
            return
        } else {
            message = game.i18n.format("tor2e.sheet.actions.rest.prolonged.restoredMessage", {
                name: this.actor.name
            });
        }

        await this.actor.update({"data.resources.endurance.value": enduranceAfterRest});

        if (!this.actor.shouldBeWeary() && this.actor.getWeary()) {
            await this.actor.toggleStatusEffectById(StatusEffects.WEARY)
        }

        await chatMessage.createRestMessage(this.actor, message, img)
    },

    "onShortRest": async function (extra = {}, event) {
        event.preventDefault();
        let message;
        let chatMessage = new Tor2eChatMessage();

        await _recoverHopePoint(this.actor);

        const img = {
            src: "systems/tor2e/assets/images/icons/rest/campfire.svg",
            title: "tor2e.sheet.actions.rest.short.title",
            alt: "tor2e.sheet.actions.rest.short.label"
        }

        if (this.actor.isWounded()) {
            message = game.i18n.format("tor2e.sheet.actions.rest.short.woundedRestoredMessage", {
                name: this.actor.name
            });
            await chatMessage.createRestMessage(this.actor, message, img)
            return
        }

        let strength = this.actor.data.data.attributes.strength.value;

        let currentEndurance = this.actor.data.data.resources.endurance.value;
        let restoredEndurance = currentEndurance + strength;
        let maxEndurance = this.actor.data.data.resources.endurance.max;

        let enduranceAfterRest = restoredEndurance > maxEndurance ? maxEndurance : restoredEndurance;

        if (enduranceAfterRest - currentEndurance > 0) {
            this.actor.update({"data.resources.endurance.value": enduranceAfterRest});

            if (!this.actor.shouldBeWeary() && this.actor.getWeary()) {
                await this.actor.toggleStatusEffectById(StatusEffects.WEARY)
            }

            message = game.i18n.format("tor2e.sheet.actions.rest.short.restoredMessage", {
                name: this.actor.name,
                amount: enduranceAfterRest - currentEndurance,
                endurance: enduranceAfterRest
            });
        } else {
            message = game.i18n.format("tor2e.sheet.actions.rest.short.noRestoredMessage", {
                name: this.actor.name
            });
        }

        await chatMessage.createRestMessage(this.actor, message, img);
    }

}